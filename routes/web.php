<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;



Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard/settings/index', 'SettingsController@index');
Route::get('/dashboard/settings/edit', 'SettingsController@update');
Route::get('/user', function ()
{
	return view('profile');
});
Route::get('/dashboard/experience/add', 'ExperienceController@create');

Route::post('/dashboard/experience/store', 'ExperienceController@store');

Route::get('/dashboard/experience/index', 'ExperienceController@index');
Route::get('/dashboard/experience/{id}/edit', 'ExperienceController@edit');
Route::get('/dashboard/experience/{id}/details', 'ExperienceController@show');

Route::post('/dashboard/experience/{id}/update', 'ExperienceController@update');
Route::post('/dashboard/experience/{id}/delete', 'ExperienceController@destroy');

/*
==============================================
For authentication purpose + Registration
==============================================
*/

Route::get('/logout', 'dashboardController@logout');
Route::get('/dashboard', 'dashboardController@showDashboard');
Route::get('/registration', 'dashboardController@newUserRegistration' );

/*===================================
Verification Purposes
====================================*/
Route::get('verify', 'VerificationController@index'); 
Route::post('/verifyuser', 'VerificationController@UserVerification');
Route::get ('/verifyuser', 'VerificationController@UserVerification');
Route::post('/newverification', 'VerificationController@NewVerificationCode');

/*====================================
For Facts
====================================*/
Route::get('/dashboard/fact/create', 'FactController@create');
Route::get('/dashboard/fact', 'FactController@index');

Route::get('/dashboard/fact/edit/{id}', 'FactController@edit');
Route::post('/dashboard/fact/store', 'FactController@store');
Route::post('/dashboard/fact/{id}/update/', 'FactController@update');
Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/404', function ()
{
	return view('404');
});
