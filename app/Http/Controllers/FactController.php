<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use File;
use App\Fact;
use Auth;
use Illuminate\Support\Facades\Session;

/*==========================================
Date : 05/03/2017
by coder618
Fact Controller with image upload
==========================================*/

class FactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
    	return view('Admin.fact.create');
    }
    public function store(Request $request)
    {
        //Validate the form
        $this->validate($request, [
            'image' => 'required',
            'title' => 'required|min:4|unique:facts',
            'no_of_items' => 'required'
        ]);

    	
        $files = $request->file('image');
        $extension = File::extension( $files->getClientoriginalName() );
        $generated_fname = uniqid().".".$extension;

        if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif'  ) {

            Storage::put($generated_fname, file_get_contents($files) );

            $data = $request->all();
            $data['user_id'] = Auth::id();
            $data['image'] = $generated_fname;
            Fact::create($data);

            Session::flash('message-success','Facts Successfully Added');
            return redirect('/dashboard/fact/create');


        }else{
            echo "not a valid file formate";
            Session::flash('message-error', 'File Formate not support');
            return redirect('/dashboard/fact/create');


        }

    }


    public function index()
    {
        $data = Fact::all();
        return view('Admin.fact.index', ['allFacts' => $data ]);
        
    }
    public function edit($id)
    {
        $data = Fact::find($id);
        return view('Admin.fact.edit', compact('data'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            // 'image' => 'required',
            'title' =>'required|min:4',
            'no_of_items' => 'required'
        ]);
        $data = $request->all();


        $files = $request->file('image');

        if (!empty($files)) {
            // If new file selected for upload
            $extension = File::extension( $files->getClientoriginalName() );
            $generated_fname = uniqid().".".$extension;

            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif'  ) {

                Storage::put($generated_fname, file_get_contents($files) );


                $data['user_id'] = Auth::id();
                $data['image'] = $generated_fname;

                $mydata = Fact::find($id);
                $mydata->update($data);
                

                Session::flash('message-success','Facts Successfully Updated with image');
                return redirect('/dashboard/fact/');


            }else{
                echo "not a valid file formate";
                Session::flash('message-error', 'File Formate not support');
                return redirect('/dashboard/fact/create');
            }
            echo "New file select";
        }else{
            // If no file selected for upload
            
            $mydata = Fact::find($id);

            $data['user_id'] = Auth::id();
            $data['image'] = $mydata->image;

            $mydata->update($data);
            Session::flash('message-success','Facts Successfully Updated with previous');
            return redirect('/dashboard/fact/create');

            echo "No file select";
        }



















        $mydata = Fact::find($id);
        $mydata->update($data);

        
    }




}
