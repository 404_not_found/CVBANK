<?php

namespace App\Http\Controllers;
use App\Setting;
use Illuminate\Http\Request;
use Auth;
class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $data = Setting::all();
        return view('Admin.setting.index', ['alldata' => $data ]);

    }
    public function edit($id)
    {
        $data = Setting::find($id);
        return view('Admin.fact.edit', compact('data'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            // 'image' => 'required',
            'title' =>'required|min:4',
            'no_of_items' => 'required'
        ]);
        $data = $request->all();


        $files = $request->file('image');

        if (!empty($files)) {
            // If new file selected for upload
            $extension = Setting::extension( $files->getClientoriginalName() );
            $generated_fname = uniqid().".".$extension;

            if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'gif'  ) {

                Storage::put($generated_fname, file_get_contents($files) );


                $data['user_id'] = Auth::id();
                $data['image'] = $generated_fname;

                $mydata = Fact::find($id);
                $mydata->update($data);


                Session::flash('message-success','Facts Successfully Updated with image');
                return redirect('/dashboard/fact/');


            }else{
                echo "not a valid file formate";
                Session::flash('message-error', 'File Formate not support');
                return redirect('/dashboard/fact/create');
            }
            echo "New file select";
        }else{
            // If no file selected for upload

            $mydata = Setting::find($id);

            $data['user_id'] = Auth::id();
            $data['image'] = $mydata->image;

            $mydata->update($data);
            Session::flash('message-success','Facts Successfully Updated with previous');
            return redirect('/dashboard/fact/create');

            echo "No file select";
        }



















        $mydata = Fact::find($id);
        $mydata->update($data);


    }

}
