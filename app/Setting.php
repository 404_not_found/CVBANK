<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;
    protected  $fillable = ['id', 'tile', 'fullname', 'description', 'address','featured_img', 'user_id' ];
    protected $dates=['deleted-at'];
}
