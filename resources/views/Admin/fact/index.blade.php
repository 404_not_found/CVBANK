@extends('Admin.master')

@section('title', 'My fact')
@section('sub-title','My Fact')

@section('content')

@include('layouts.include.sessionmessage')

<table class="table table-bordered">
        <tr>
            <td>ID</td>
            <td>TITLE</td>
            <td>Description</td>
            <td>Action</td>
        </tr>
        @foreach($allFacts as $service)
            <tr>
                <td>{{ $service->id }}</td>
                <td>{{ $service->title }}</td>
                <td>{{ $service->image }}</td>
                <td>View</td>
                <td><a href="{{url("/dashboard/fact/edit/$service->id")}}">Edit</a></td>
                <td>Delete</td>
            </tr>

        @endforeach
    </table>








@endsection