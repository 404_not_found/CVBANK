@extends('Admin.master')

@section('title', 'Update Settings')
@section('sub-title','Update Settings')

@section('in-head')
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/form_layouts.js')}}"></script>
@endsection


@section('content')


    <div class="col-md-offset-3 col-md-6">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title" style="text-align: center;">Update Settings</h6>
            </div>

            <div class="panel-body">

                @include('layouts.include.errors')
                @include('layouts.include.sessionmessage')

                {{ Form::open(['url' => '/dashboard/settings/'.$data['id'].'/update' ,'files'=>'true']) }}

                <div class="form-group">
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', $data['tile'], ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('no_of_items', 'Number of items') !!}
                    {!! Form::number('no_of_items', $data['	fullname'], ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('no_of_items', 'Number of items') !!}
                    {!! Form::number('no_of_items', $data['	description'], ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <div class="media-body">
                        <label for="no_of_item">Fact Image</label>

                        <input type="file" name="image">

                        <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Update  <i class="icon-arrow-right14 position-right"></i></button>
                </div>


                {!! Form::close() !!}

            </div>
        </div>
    </div>


@endsection