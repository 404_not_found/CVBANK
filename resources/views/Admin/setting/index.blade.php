@extends('Admin.master')
@section('title', 'My Settings')
@section('sub-title','My Settings')
@section('content')
    @include('layouts.include.sessionmessage')

    <table class="table table-bordered">
        <tr>
            <td>ID</td>
            <td>TITLE</td>
            <td>FULL NAME</td>
            <td>Description</td>
            <td>Featured Image</td>
            <td>Action</td>
        </tr>
        @foreach($alldata as $setting)
            <tr>
                <td>@if(empty($setting->id))
                {{ 'Not Set Yet' }}
                    @endif
                    @if(!empty($setting->id))
                    {{$setting->id}}
                    @endif
                 </td>
                <td>@if(empty($setting->tile))
                        {{ 'Not Set Yet' }}
                    @endif
                    @if(!empty($setting->tile))
                        {{$setting->tile}}
                    @endif
                </td>
                <td>@if(empty($setting->fullname))
                        {{ 'Not Set Yet' }}
                    @endif
                    @if(!empty($setting->fullname))
                        {{$setting->fullname}}
                    @endif
                </td>
                <td>@if(empty($setting->description))
                        {{ 'Not Set Yet' }}
                    @endif
                    @if(!empty($setting->description))
                        {{$setting->description}}
                    @endif
                </td>

                <td>@if(empty($setting->featured_img))
                        {{ 'Not Set Yet' }}
                    @endif
                    @if(!empty($setting->featured_img))
                        {{$setting->featured_img}}
                    @endif
                </td>

                <td>View</td>

            </tr>

        @endforeach
    </table>








@endsection